//
//  LocationToTrackViewController.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 02/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import UIKit
import CoreGraphics

enum LocationsToTrackState{
    case fullView
    case slidedDown
}

protocol LocationsToTrackDelegate: class {
    var ownerViewController: LocationViewController { get }
    func userSelectedRowAtTrackLocation(trackPlace: TrackPlace)
    func changedViewControllerState(to state: LocationsToTrackState) -> LocationsToTrackState?
    func userDeletedTrackPlace(trackPlace: TrackPlace) -> Bool
    func userEditedTrackPlace(trackPlaceHash: Int, to trackPlace: TrackPlace) -> Bool
    func toggleEditNavigationEditButton(isActive: Bool)
}

class LocationsToTrackViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    var dataSource: TrackPlaceStruct? {
        didSet{
            if oldValue == nil{
                tableView.reloadData()
            }
        }
    }
    
    weak var delegate: LocationsToTrackDelegate? {
        didSet{
            guard let f = delegate?.ownerViewController.navigationController?.navigationBar.frame.height, let s = delegate?.ownerViewController.view.frame.size.height, let tabbarHeight = delegate?.ownerViewController.tabBarController?.tabBar.frame.size.height else{
                return
            }
            self.fullViewViewPosition = f + UIApplication.shared.statusBarFrame.height
            self.slidedDownViewPosition = s - tabbarHeight - UIApplication.shared.statusBarFrame.height - 50
            self.changeViewControllerValueTo(to: actualViewControllerState)
            actualViewControllerState = .slidedDown
        }
    }
    
    var actualViewControllerState: LocationsToTrackState = .slidedDown {
        willSet{
            changeViewControllerValueTo(to: newValue)
        }
        didSet{
            if delegate?.changedViewControllerState(to: actualViewControllerState) == oldValue{
                changeViewControllerValueTo(to: oldValue)
            }else{
                if actualViewControllerState == .slidedDown{
                    tableView.isUserInteractionEnabled = false
                    tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRec(_:)))
                    if let tapGest = tapGestureRecognizer{
                        self.view.addGestureRecognizer(tapGest)
                    }
                    delegate?.toggleEditNavigationEditButton(isActive: false)
                    startAnimationPosition = slidedDownViewPosition ?? 0
                }
                if actualViewControllerState == .fullView{
                    tableView.isUserInteractionEnabled = true
                    if let tapGest = tapGestureRecognizer{
                        self.view.removeGestureRecognizer(tapGest)
                    }
                    delegate?.toggleEditNavigationEditButton(isActive: true)
                    startAnimationPosition = fullViewViewPosition ?? 0
                }
            }
        }
    }
    var startAnimationPosition: CGFloat = 0
    
    var slidedDownViewPosition: CGFloat?
    var fullViewViewPosition: CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 15
        
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: 15).cgPath
//        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        view.layer.shadowOpacity = 0.5
//        view.layer.shadowRadius = 10
        
        tableView.tableHeaderView = UINib(nibName: "TableViewHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? UIView
        
        let panGest = UIPanGestureRecognizer(target: self, action: #selector(panGestRec(_:)))
        self.tableView.tableHeaderView?.addGestureRecognizer(panGest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ds = dataSource, ds.data.count > 0{
            return ds.data.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        
        guard let dataSource = dataSource, dataSource.data.count > indexPath.row, dataSource.data.count > 0 else {
            cell.textLabel?.text = "Brak lokalizacji"
            return cell
        }
        cell.textLabel?.text = (dataSource.data[indexPath.row].name == "") ? "Brak nazwy lokalizacji" : dataSource.data[indexPath.row].name
        
        return cell
    }
    
    func changeViewControllerValueTo(to state: LocationsToTrackState = .slidedDown){
        guard let fullViewViewPosition = self.fullViewViewPosition, let slidedDownViewPosition = self.slidedDownViewPosition else { return }
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            switch state{
            case .fullView:
                self.view.frame.origin.y = fullViewViewPosition
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layer.cornerRadius = 0
                })
            case .slidedDown:
                self.view.frame.origin.y = slidedDownViewPosition
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layer.cornerRadius = 15
                    if let image = self.tableView.tableHeaderView?.subviews[0] as? UIImageView{
                        UIView.transition(with: image, duration: 0.3, options: .transitionCrossDissolve, animations: {
                            image.image = #imageLiteral(resourceName: "singleLine")
                        }, completion: nil)
                        
                    }
                })
            }
        }) { _ in }
    }
    

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let ds = self.dataSource?.data, ds.count > indexPath.row else { return nil }
        guard let cell = self.tableView.cellForRow(at: indexPath), cell.textLabel?.text != "Brak lokalizacji" else { return nil }
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Usuń") { (action, ip) in
            if self.delegate?.userDeletedTrackPlace(trackPlace: ds[indexPath.row]) == true{
                self.tableView.deleteRows(at: [ip], with: .automatic)
            }else{
                cell.textLabel?.text = "Brak lokalizacji"
            }
        }
        let editAction = UITableViewRowAction(style: .normal, title: "Edytuj") { (action, ip) in
            let alert = UIAlertController(title: "Zmień nazwę", message: "Wpisz nową nazwę miejsca", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addTextField { (textField) in
                textField.text = cell.textLabel?.text ?? "Brak lokalizacji"
            }

            let action = UIAlertAction(title: "Zmień", style: .default, handler: { (action) in
                let textField = alert.textFields![0] as UITextField
                guard let oldTrackPlace = self.dataSource?.data[indexPath.row] else { return }
                var newTrackPlace = oldTrackPlace
                newTrackPlace.name = textField.text ?? "Brak nazwy miejsca"
                if self.delegate?.userEditedTrackPlace(trackPlaceHash: (self.dataSource?.data[indexPath.row].hashValue) ?? 0, to: newTrackPlace) == true{
                    self.dataSource?.data[indexPath.row] = newTrackPlace
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                }else{
                    let alert2 = UIAlertController(title: "Wystąpił błąd :/", message: "Wystąpił błąd podczas zmiany nazwy, spróbuj ponownie", preferredStyle: UIAlertControllerStyle.alert)
                    let action2 = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) in
                        
                    })
                    alert2.addAction(action2)
                    self.present(alert2, animated: true, completion: nil)
                }
                
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
        }
        editAction.backgroundColor = .blue
        return [deleteAction, editAction]

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.dataSource?.data.count ?? 0 > indexPath.row else { return }
        if let data = dataSource?.data{
            delegate?.userSelectedRowAtTrackLocation(trackPlace: data[indexPath.row])
        }
    }
    
    @objc func tapGestureRec(_ sender: UITapGestureRecognizer){
        if self.actualViewControllerState == .slidedDown{
            self.actualViewControllerState = .fullView
        }
    }
    @objc func panGestRec(_ sender: UIPanGestureRecognizer){
        let trans = sender.translation(in: delegate?.ownerViewController.view).y
        func calculateNewStateForView(){
            let middlePoint = ((slidedDownViewPosition ?? 0) + (fullViewViewPosition ?? 0)) / 2
            if trans + self.view.frame.origin.y > middlePoint{
                actualViewControllerState = .slidedDown
                if let image = self.tableView.tableHeaderView?.subviews[0] as? UIImageView{
                    UIView.transition(with: image, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        image.image = #imageLiteral(resourceName: "singleLine")
                    }, completion: nil)
                }
            }else{
                actualViewControllerState = .fullView
            }
        }
        
        UIView.animate(withDuration: 0.1) {
            self.view.frame.origin.y = trans + self.startAnimationPosition
        }
        if self.view.layer.cornerRadius == 0{
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layer.cornerRadius = 15
            })
        }
        if let image = self.tableView.tableHeaderView?.subviews[0] as? UIImageView{
            UIView.transition(with: image, duration: 0.3, options: .transitionCrossDissolve, animations: {
                image.image = #imageLiteral(resourceName: "bottomArrow")
            }, completion: nil)
        }
        if sender.state == .ended{
            calculateNewStateForView()
        }
        
    }


    
}
