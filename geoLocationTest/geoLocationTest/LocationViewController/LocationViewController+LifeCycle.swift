//
//  LocationViewController+LifeCycle.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 27/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit



extension LocationViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationControllerProperties()
        searchBar.delegate = self
        
        setTextField()
        
        setMapView()
        
        addLocationsToTrackVC()
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.userLocationWasSet = false
    }
    
    
}
