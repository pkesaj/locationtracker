//
//  SearchSuggestionView.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 30/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import MapKit

protocol SearchSuggestionViewCommunitaction: class{
    func userSelectedMapItem(_ mapItem: MKMapItem)
}

class SearchSuggestionView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [MKMapItem]?
    
    var delegate: SearchSuggestionViewCommunitaction?
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
    }
 

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        guard let name = self.dataSource?[indexPath.row].name else {
            cell.textLabel?.text = "Brak wyników"
            return cell
            
        }
        
        cell.textLabel?.text = name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard (dataSource?.count ?? 0) > indexPath.row, let mapItem = dataSource?[indexPath.row] else { return }
        self.delegate?.userSelectedMapItem(mapItem)
        
        self.removeFromSuperview()
    }
    
    
}
