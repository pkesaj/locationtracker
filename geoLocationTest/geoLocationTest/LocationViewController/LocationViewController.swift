//
//  LocationViewController.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 27/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationViewController: UIViewController {
    
    weak var locationsToTrackVC: LocationsToTrackViewController?
    
    @IBAction func addNewLocationToTrack(_ sender: UIBarButtonItem) {
        self.searchBar.isHidden = !self.searchBar.isHidden
    }
    
    @IBOutlet weak var mapView: MKMapView!
    var middleAnnotation: MKPlacemark?
    var middleCircle: MKCircle?
    
    @IBOutlet weak var searchBar: UISearchBar!
    weak var searchView: SearchSuggestionView?
    var searchTimer: Timer?
    var selectedMapItem: MKMapItem? {
        didSet{
            guard let s = selectedMapItem else { return }
            self.setRegionToSearchedMapItem(s)
        }
    }
    
    
    @IBOutlet weak var addNewItemView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cancelButtonOutLet: UIButton!
    @IBOutlet weak var addButtonOutlet: UIButton!
    @IBAction func cancelButtonAction(_ sender: Any) {
        nameTextField.text = ""
        addNewItemView.isHidden = true
    }
    @IBAction func addNewPlaceAction(_ sender: Any) {
        guard let text = nameTextField.text, let middleAnnotation = middleAnnotation else { return }
        addNewPlaceToTrack(placemark: middleAnnotation, name: text, radius: 1000)
    }
    func setAddButtonState(active: Bool){
        if active{
            addButtonOutlet.isEnabled = true
        }else{
            addButtonOutlet.isEnabled = false
        }
    }
    
    
    
    
    
    var userLocationWasSet: Bool = false
    
    //properties
    
    let locationManager = CLLocationManager()
    

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.searchBar.isFirstResponder{
            self.searchBar.resignFirstResponder()
        }
        if nameTextField.isFirstResponder{
            self.nameTextField.resignFirstResponder()
        }
        
        
    }
    
}
