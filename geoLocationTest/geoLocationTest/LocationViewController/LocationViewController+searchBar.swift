//
//  LocationViewController+searchBar.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 30/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import MapKit


extension LocationViewController: UISearchBarDelegate, SearchSuggestionViewCommunitaction{

    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let timer = searchTimer{
            timer.invalidate()
        }
        searchTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer) in
            guard let text = self.searchBar.text, text != " " else {
                self.showSuggestSearchView(to: nil)
                return
            }
            self.showSuggestSearchView(to: text)
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        guard self.searchBar.text != nil else {
            self.searchBar.text = ""
            return
        }
        self.searchBar.text!.forEach{ _ in
            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false, block: { (_) in
                self.searchBar.text?.removeLast()
            })
            
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = self.searchBar.text, text != " " else {
            self.showSuggestSearchView(to: nil)
            return
        }
        self.showSuggestSearchView(to: text)
        self.searchBar.resignFirstResponder()
        self.searchTimer?.invalidate()
    }
    
    func userSelectedMapItem(_ mapItem: MKMapItem) {
        self.searchBar.isHidden = true
        self.searchView?.removeFromSuperview()
        self.selectedMapItem = mapItem
    }
    
    
}
