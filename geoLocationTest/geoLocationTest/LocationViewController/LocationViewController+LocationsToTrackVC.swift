//
//  LocationViewController+LocationsToTrackVC.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 02/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import UIKit
import MapKit

extension LocationViewController: LocationsToTrackDelegate {

    
    var ownerViewController: LocationViewController {
        get {
            return self
        }
    }
    
    func userSelectedRowAtTrackLocation(trackPlace: TrackPlace) {
        self.locationsToTrackVC?.actualViewControllerState = .slidedDown
        self.mapView.setRegion(MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2D(latitude: trackPlace.latitude, longitude: trackPlace.longtitude), 5000, 5000), animated: true)
    }
    
    func changedViewControllerState(to state: LocationsToTrackState) -> LocationsToTrackState? {
        return nil
    }
    
    func userDeletedTrackPlace(trackPlace: TrackPlace) -> Bool {
        if let index = DatabaseManager.i.trackedPlaces.data.index(where: { $0 == trackPlace }){
            var t = DatabaseManager.i.trackedPlaces
            t.data.remove(at: index)
            self.locationsToTrackVC?.dataSource?.data.remove(at: index)
            DatabaseManager.i.trackedPlaces = t
            if DatabaseManager.i.trackedPlaces.data.count > 0{
                return true
            }else{ // we cannot delete all rows.
                return false
            }
        }else{
            return false
        }
    }
    
    func userEditedTrackPlace(trackPlaceHash: Int, to trackPlace: TrackPlace) -> Bool {
        if let i = DatabaseManager.i.trackedPlaces.data.index(where: { $0.hashValue == trackPlaceHash }){
            DatabaseManager.i.trackedPlaces.data[i] = trackPlace
            return true
        }else{
            return false
        }
    }
    

    
    func addLocationsToTrackVC(){
        let dataSource = DatabaseManager.i.trackedPlaces
        guard self.locationsToTrackVC == nil else {
            self.locationsToTrackVC?.dataSource = dataSource
            self.locationsToTrackVC?.tableView.reloadData()
            return
        }
        guard let lvc = UIStoryboard(name: "LocationsToTrackStoryboard", bundle: nil).instantiateInitialViewController() as? LocationsToTrackViewController else { return }
        
        self.addChildViewController(lvc)
        self.view.addSubview(lvc.view)
        lvc.delegate = self
        lvc.dataSource = dataSource
        self.locationsToTrackVC = lvc
        
        
    }
    
    @objc func locationsVCToggleEditButton(){
        self.locationsToTrackVC?.tableView.setEditing(!(self.locationsToTrackVC?.tableView.isEditing ?? false), animated: true)
        if self.locationsToTrackVC?.tableView.isEditing ?? false{
            navigationItem.leftBarButtonItem?.title = "Done"
        }else{
            navigationItem.leftBarButtonItem?.title = "Edit"
        }
    }
    
    func toggleEditNavigationEditButton(isActive: Bool) {
        if !isActive{
            navigationItem.leftBarButtonItem = nil
            self.locationsToTrackVC?.tableView.setEditing(false, animated: true)
        }else{
            let editButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(locationsVCToggleEditButton))
            navigationItem.leftBarButtonItem = editButton
        }
    }

}
