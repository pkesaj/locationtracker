//
//  LocationViewController+mapManager.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 27/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

extension LocationViewController{
    
    func setMapView(){
        guard CLLocationManager.authorizationStatus() == .authorizedAlways else {
            locationManager.requestAlwaysAuthorization()
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { _ in self.setMapView()})
            return
        }
        self.mapView.showsUserLocation = true
        self.mapView.showsPointsOfInterest = true
        self.mapView.isZoomEnabled = true
        
        mapView.delegate = self
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(mapViewTapped(_:)))
        mapView.addGestureRecognizer(tapGest)
        
    }
    
    func showSuggestSearchView(to find: String? = nil){
        guard let find = find else{
            self.mapView.zoomToUserLocation()
            return
        }

        let locationSearchRequest = MKLocalSearchRequest()
        locationSearchRequest.naturalLanguageQuery = find
        locationSearchRequest.region = self.mapView.region
        
        MKLocalSearch(request: locationSearchRequest).start(completionHandler: {response, error in
            if self.searchView == nil{
                guard let searchView = UINib(nibName: "SearchSuggestionsView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? SearchSuggestionView else {
                    return
                }
                self.view.addSubview(searchView)
                self.searchView = searchView
            }
            guard let searchView = self.searchView else { return }
            if let dataSource = response?.mapItems {
                searchView.dataSource = dataSource
            }
            searchView.delegate = self
            searchView.tableView.reloadData()
            
            searchView.frame = CGRect(x: 0, y: self.searchBar.frame.size.height + self.searchBar.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height - self.searchBar.frame.size.height + self.searchBar.frame.origin.y)
            
        })
    }
    
    func setRegionToSearchedMapItem(_ mapItem: MKMapItem){
        guard let map = self.mapView else { return }
    
        let region = MKCoordinateRegion(center: mapItem.placemark.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03))
        map.setRegion(region, animated: true)
        if middleAnnotation == nil {
            middleAnnotation = mapItem.placemark
        }
        // dodajemy adnotacje na mapie jesli takowa nie istnieje, jesli istnieje to nie dodajemy.
        guard let m = middleAnnotation, self.mapView.annotations.index(where: { $0.hash == m.hash }) != nil else{
            self.mapView.addAnnotation(mapItem.placemark)
            return
        }
    }
   
    func updateTrackLocations(){
        DispatchQueue.main.async {
            let trackPoints = DatabaseManager.i.trackedPlaces

            trackPoints.data.forEach{ trackPoint in
                CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: trackPoint.latitude, longitude: trackPoint.longtitude), completionHandler: { (placemarks, error) in
                    guard error == nil, let placemark = placemarks?.first else { return }
                    let mkPlacemark = MKPlacemark(placemark: placemark)
                    let circle = MKCircle(center: mkPlacemark.coordinate, radius: trackPoint.radius)
                    self.mapView.addAnnotation(mkPlacemark)
                    self.mapView.add(circle)
                })
            }
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.removeOverlays(self.mapView.overlays)
            
            self.updateMonitoringLocations()
        }
    }
    
}


extension LocationViewController: MKMapViewDelegate{
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        if !userLocationWasSet {
            self.mapView.zoomToUserLocation()
            self.updateTrackLocations()
            userLocationWasSet = true
        }
        
    }
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if let m = middleAnnotation{
            
            mapView.removeAnnotation(m)
            middleAnnotation = nil
        }
        if let c = middleCircle{
            mapView.remove(c)
            middleCircle = nil
        }
        if self.addNewItemView.isHidden == false{
            self.addNewItemView.isHidden = true
        }
        if self.nameTextField.isFirstResponder{
            self.nameTextField.resignFirstResponder()
        }
        
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = .red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKOverlayRenderer()
        }
    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let newAnn = MKPlacemark(coordinate: mapView.centerCoordinate)
        
        mapView.addAnnotation(newAnn)
        middleAnnotation = newAnn
        let circle = MKCircle(center: newAnn.coordinate, radius: 1000)
        mapView.add(circle)
        self.middleCircle = circle
        
    }
    
    @objc func mapViewTapped(_ sender: UITapGestureRecognizer){
        self.addNewItemView.isHidden = false
        if let text = self.nameTextField.text, text.count != 0{
            setAddButtonState(active: true)
      
        }else{
            setAddButtonState(active: false)
            DispatchQueue.main.async {
                guard let middleAnnotation = self.middleAnnotation else { return }
                CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: middleAnnotation.coordinate.latitude, longitude: middleAnnotation.coordinate.longitude), completionHandler: { (placemarks, error) in
                    guard self.nameTextField.text?.count == 0, let name = placemarks?.first?.name else { return }
                    
                    self.nameTextField.text = name
                    self.setAddButtonState(active: true)
                })
            }
        }
    }
    
    func startMonitoringLocations(){
        let trackPoints = DatabaseManager.i.trackedPlaces
        trackPoints.data.forEach{ trackPoint in
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: trackPoint.latitude, longitude: trackPoint.longtitude), radius: trackPoint.radius, identifier: trackPoint.name)
            locationManager.startMonitoring(for: region)
        }
    }
    
    func stopMonitoringLocations(){
        let regions = locationManager.monitoredRegions
        regions.forEach{
            locationManager.stopMonitoring(for: $0)
        }
    }
    
    func updateMonitoringLocations(){
        stopMonitoringLocations()
        startMonitoringLocations()
    }
    
}
