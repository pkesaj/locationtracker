//
//  LocationViewController+AddNewPlace.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 30/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import MapKit



extension LocationViewController: UITextFieldDelegate{
    func setTextField(){
        nameTextField.layer.masksToBounds = true
        nameTextField.layer.cornerRadius = 5
        nameTextField.delegate = self
        nameTextField.addTarget(self, action: #selector(nameTextFieldValueChanged(_:)), for: .valueChanged)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @objc func nameTextFieldValueChanged(_ sender: UITextField){
        if let text = sender.text{
            if text.count > 0{
                setAddButtonState(active: true)
            }else{
                setAddButtonState(active: false)
            }
        }else{
            setAddButtonState(active: false)
        }
    }
    
    // defaultowo 1000m
    func addNewPlaceToTrack(placemark: MKPlacemark, name: String, radius: Double = 1000){
        let trackPlace = TrackPlace(name: name, lat: placemark.coordinate.latitude, lon: placemark.coordinate.longitude, radius: radius)
        DatabaseManager.i.trackedPlaces.data.append(trackPlace)
        updateTrackLocations()
        addLocationsToTrackVC()
    }
    
    
}
