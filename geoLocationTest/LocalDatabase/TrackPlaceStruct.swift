//
//  TrackPlaceStruct.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 30/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import Foundation




struct TrackPlaceStruct: Codable{
    var data: [TrackPlace]
    
    init(data: [TrackPlace]) {
        self.data = data
    }
}

struct TrackPlace: Codable, Hashable{

    
    var name: String
    var latitude: Double
    var longtitude: Double
    var radius: Double
    
    var locationHistory: [LocationHistoryStruct]?
    
    enum CodingKeys: String, CodingKey{
        case name, latitude, longtitude, radius, locationHistory
    }
    
    init(name: String, lat: Double, lon: Double, radius: Double) {
        self.name = name
        self.latitude = lat
        self.longtitude = lon
        self.radius = radius
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longtitude, forKey: .longtitude)
        try container.encode(radius, forKey: .radius)
        try container.encode(locationHistory, forKey: .locationHistory)
    }
    
    var hashValue: Int {
        return name.hashValue
            ^ latitude.hashValue
            ^ longtitude.hashValue
            ^ radius.hashValue
            ^ (locationHistory?.reduce(0, { (result, locHist) -> Int in
                return result + locHist.hashValue
            }) ?? 0) / 2
    }
    
    func getHashRegion() -> Int{
        return self.latitude.hashValue
        ^ self.longtitude.hashValue
        ^ self.radius.hashValue
    }
    
    static func ==(lhs: TrackPlace, rhs: TrackPlace) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
}
