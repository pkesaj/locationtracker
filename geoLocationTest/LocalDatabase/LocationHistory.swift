//
//  LocationHistory.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 02/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import Foundation


struct LocationHistoryStruct: Codable, Hashable{
    var enterTime: Double
    var exitTime: Double // jeśli jest to zero wtedy jest on w tej lokalizacji wciąż i przy starcie wykres wie gdzie sie ma skonczyc
    
    var hashValue: Int {
        return enterTime.hashValue ^ exitTime.hashValue
    }
    
    static func ==(lhs: LocationHistoryStruct, rhs: LocationHistoryStruct) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    
    
    
    
    
}
