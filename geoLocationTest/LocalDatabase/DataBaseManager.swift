//
//  DataBaseManager.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 30/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import Foundation


class DatabaseManager{
    
    private let keyForTrackedPlaces = "trackedPlaces"
    
    public static let sharedInstance = DatabaseManager()
    public static let i = sharedInstance
    
    var trackedPlaces: TrackPlaceStruct = TrackPlaceStruct(data: [TrackPlace(name: "", lat: 0, lon: 0, radius: 0)]) {
        didSet{
            updateDatabase(trackedPlaces, keyForTrackedPlaces)
        }
    }
    
    init() {
        do{
            if let t: TrackPlaceStruct = (try getDataFromDatabase(decodableProtocol: TrackPlaceStruct.self, forKey: keyForTrackedPlaces)){
                trackedPlaces = t
            }
        }catch let error{
            print(error)
        }
        
    }
    
    func getDataFromDatabase<T: Codable>(decodableProtocol: T.Type ,forKey: String) throws -> T? {
        let throwError = NSError(domain: "decodingError", code: 0, userInfo: nil)
        guard let data = UserDefaults.standard.object(forKey: keyForTrackedPlaces) as? Data else { throw throwError }
        do {
            let decodedData = try JSONDecoder().decode(decodableProtocol, from: data)
            return decodedData
        }catch let error{
            print(error)
            return nil
        }
        
    }
    
    func updateDatabase<T: Codable>(_ value: T,_ key: String){
        do{
            let decodedData = try JSONEncoder().encode(value)
            UserDefaults.standard.set(decodedData, forKey: key)
        }catch let error{
            print(error)
        }
    }
    
}
