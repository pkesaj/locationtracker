//
//  AppDelegate.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 27/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region, didEnter: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region, didEnter: false)
        }
    }
    
    func handleEvent(forRegion region: CLRegion!, didEnter: Bool) {
        guard let region = region as? CLCircularRegion else { return }
        let regionHash = region.center.latitude.hashValue ^ region.center.longitude.hashValue ^ region.radius.hashValue
        guard let i = DatabaseManager.i.trackedPlaces.data.index(where: { $0.getHashRegion() == regionHash })else{ return }
        let timeStamp: Double = Date(timeIntervalSince1970: 0).timeIntervalSince1970
        var placeToEdit = DatabaseManager.i.trackedPlaces.data[i]
        
        if didEnter{ // did enter
            guard placeToEdit.locationHistory?.last?.exitTime != 0 else { return }
            placeToEdit.locationHistory?.append(LocationHistoryStruct(enterTime: timeStamp, exitTime: 0))
        }else{ // did exit
            guard placeToEdit.locationHistory?.last?.enterTime != 0, placeToEdit.locationHistory?.last?.enterTime != nil else { return }
            var locHist = placeToEdit.locationHistory!
            locHist.removeLast()
            locHist.append(LocationHistoryStruct(enterTime: (placeToEdit.locationHistory?.last?.enterTime) ?? 0, exitTime: timeStamp))
            placeToEdit.locationHistory = locHist
        }
        DatabaseManager.i.trackedPlaces.data[i] = placeToEdit
    }
    
}

