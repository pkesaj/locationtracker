//
//  zoomToLocation+MKMapView.swift
//  geoLocationTest
//
//  Created by Piotr Krzesaj on 27/12/2017.
//  Copyright © 2017 pkTeam. All rights reserved.
//

import MapKit


extension MKMapView{
    
    func zoomToUserLocation() {
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000)
        setRegion(region, animated: true)
    }
    
}
